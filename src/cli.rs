use clap::{crate_authors, crate_description, crate_name, crate_version, App, Arg};
pub fn build_cli() -> App<'static, 'static> {
    App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("DAT_FILE")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("ROM_FOLDER")
                .help("Sets the input file to use")
                .required(true)
                .index(2),
        )
        .arg(
            Arg::with_name("remove-duplicates")
                .help("Removes duplicate ROMs")
                .long("remove-duplicates"),
        )
}
