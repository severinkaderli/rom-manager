extern crate clap;
extern crate crc32fast;
extern crate indicatif;
extern crate serde_xml_rs;

mod cli;
mod model;

use crc32fast::Hasher;
use indicatif::{ProgressBar, ProgressStyle};
use model::Datafile;
use serde_xml_rs::from_reader;
use std::collections::HashSet;
use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};

fn read_dat(dat_file: &Path) -> Datafile {
    let mut xml_file = fs::File::open(dat_file).expect("File not found!");
    let mut xml = String::new();
    xml_file
        .read_to_string(&mut xml)
        .expect("Error reading file");

    let datafile: Datafile = from_reader(xml.as_bytes()).unwrap();

    return datafile;
}

fn main() {
    let matches = cli::build_cli().get_matches();

    let remove_duplicates = matches.is_present("remove-duplicates");

    let dat_file = Path::new(matches.value_of("DAT_FILE").unwrap());
    let rom_folder = Path::new(matches.value_of("ROM_FOLDER").unwrap());

    if !dat_file.is_file() {
        println!("Argument <DAT_FILE> needs to be a valid file!");
        return;
    }

    if !rom_folder.is_dir() {
        println!("Argument <ROM_FOLDER> needs to be a valid directory!");
        return;
    }

    let dat_entries = read_dat(&dat_file).get_map();

    let file_count = fs::read_dir(&rom_folder).unwrap().count();
    let roms = fs::read_dir(&rom_folder).unwrap();

    let mut unknown_folder = PathBuf::from(rom_folder.clone());
    unknown_folder.push("Unknown");

    if !unknown_folder.is_dir() {
        fs::create_dir(&unknown_folder).unwrap();
    }

    let mut hashes = HashSet::new();
    let bar = ProgressBar::new(file_count as u64);
    bar.set_style(
        ProgressStyle::default_bar()
            .template("{msg}\n[{elapsed_precise}] [{wide_bar}] [{pos}/{len}]")
            .progress_chars("=>-"),
    );

    for rom in roms {
        bar.inc(1);
        let rom_path = rom.unwrap().path();

        if !rom_path.is_file() {
            continue;
        }
        bar.set_message(rom_path.file_name().unwrap().to_str().unwrap());

        let mut hasher = Hasher::new();
        hasher.update(&fs::read(&rom_path).unwrap());
        let hash = format!("{:X}", hasher.finalize());

        if hashes.contains(&hash) {
            if remove_duplicates {
                fs::remove_file(&rom_path).expect("Can't remove file!");
            }

            continue;
        }

        if dat_entries.contains_key(&hash) {
            let new_file_name = dat_entries.get(&hash).unwrap();
            let mut new_path = rom_path.clone();
            new_path.set_file_name(new_file_name);

            fs::rename(&rom_path, new_path).expect("Error renaming file!");
        } else {
            let mut unknown_file_path = unknown_folder.clone();
            unknown_file_path.push(rom_path.file_name().unwrap());
            fs::rename(&rom_path, &unknown_file_path).expect("Error moving file!");
        }
        hashes.insert(hash);
    }
    bar.finish();
}
