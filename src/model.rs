use serde::Deserialize;
use std::collections::HashMap;

#[derive(Debug, Deserialize)]
#[serde(rename = "datafile")]
pub struct Datafile {
    #[serde(rename = "game")]
    pub games: Vec<Game>
}

#[derive(Debug, Deserialize)]
pub struct Game {
    pub name: String,
    pub description: String,
    pub rom: Rom
}

#[derive(Debug, Deserialize)]
pub struct Rom {
    pub name: String,
    pub crc: String,
    pub md5: String,
    pub sha1: String
}

impl Datafile {
    pub fn get_map(&self) -> HashMap<String, String> {
        let mut map = HashMap::new();
        for game in &self.games {
            map.insert(game.rom.crc.clone(), game.rom.name.clone());
        }

        return map;
    }
}