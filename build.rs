extern crate clap;
use std::fs;

use clap::Shell;
 
include!("src/cli.rs");
 
fn main() {
    fs::create_dir_all("./target/completions").expect("Error");
    let out_dir = "./target/completions";
    let mut app = build_cli();
    app.gen_completions(crate_name!(), Shell::Bash, &out_dir); 
    app.gen_completions(crate_name!(), Shell::Fish, &out_dir); 
    app.gen_completions(crate_name!(), Shell::Zsh, &out_dir); 
}
